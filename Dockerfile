FROM busybox:latest
RUN mkdir -p hello && \
  echo "one word" >> hello/file.txt && \
  echo "two word " >> hello/file.txt && \
  echo "hello world" >> hello/file.txt
